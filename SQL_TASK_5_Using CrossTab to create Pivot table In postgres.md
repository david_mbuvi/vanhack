# Task 5
    For this challenge you'll pivot a table.
    You're given two tables, products and details.
    Your task is to pivot the rows in products to produce a table of products which have columns of their details.
    Group and order by the name of the product.

## Table Schema
 # products
   - A table of products and their IDs 
       
    NAME                  | Type
    ----------------------+--------------
    id                    | INT
    name                  | VARCHAR(100)





# details
   - A list of details from their related products

    NAME                  | Type
    ----------------------+--------------
    id                    | INT
    product_id            | INT
    detail                | VARCHAR(5)



# Query Schema
   - Resulting Query 
      - A table of products and a count of their conditions

    NAME                  | Type
    ----------------------+--------------
    name                  | VARCHAR(100)
    good                  | INT
    ok                    | INT
    bad                   | INT



**_Compare your table to the expected table to view the expected results._**


*************************_SOLUTION QUERY_*************************

    CREATE EXTENSION tablefunc;

    -- Create your CROSSTAB statement here
    SELECT name,
       SUM(CAST(CASE WHEN detail = 'good' THEN details ELSE 0 END as INT)) AS good,
       SUM(CAST(CASE WHEN detail = 'ok' THEN details ELSE 0 END as INT)) AS ok,
       SUM(CAST(CASE WHEN detail = 'bad' THEN details ELSE 0 END as INT)) AS bad
    FROM (
        SELECT p.name AS name,
               d.detail,
               COUNT(1) AS details
          FROM products p
          JOIN details d
            ON d.product_id = p.id
         GROUP BY 1,2
       ) sub
    GROUP BY 1
    ORDER BY 1 ASC;


# Results
![Task_5_Results](Schema&Results/Task_5_Result.png)
