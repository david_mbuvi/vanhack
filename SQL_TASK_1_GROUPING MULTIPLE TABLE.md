# Task 1
    Given the the schema presented below,
    write SQL to find two actors who cast together the most and list the titles of the movies they appeared in together.
    Order the result set alphabetically.


# Table film_actor

    Column      | Type                        | Modifiers
    ------------+-----------------------------+----------
    actor_id    | smallint                    | not null
    film_id     | smallint                    | not null
    ...


# Table actor

    Column      | Type                        | Modifiers
    ------------+-----------------------------+----------
    actor_id    | integer                     | not null
    first_name  | character varying(45)       | not null
    last_name   | character varying(45)       | not null
    ...

# Table film

    Column      | Type                        | Modifiers
    ------------+-----------------------------+----------
    film_id     | integer                     | not null
    title       | character varying(255)      | not null
    ...


# The desired output:

    first_actor | second_actor | title
    ------------+--------------+--------------------
    John Doe    | Jane Doe     | The Best Movie Ever
    ...
 

- first_actor - Full name (First name + Last name separated by a space)
- second_actor - Full name (First name + Last name separated by a space)
- title - Movie title

_**Note: the actor_id of the first_actor should be lower than the actor_id of the second_actor.**_

*************************SOLUTION QUERY*************************

    select
    concat(f1.first_name, ' ', f1.last_name) as second_actor,
    concat(s2.first_name, ' ', s2.last_name) as first_actor,
    fm.title
    from (
    select
        r1.actor_id as actor_a_id,
        r2.actor_id as actor_b_id,
        count(r1.film_id) as casted_together,
        array_agg(r1.film_id) as film_ids
    from film_actor r1
    join film_actor r2
        on r1.film_id = r2.film_id
        and r1.actor_id > r2.actor_id
    join film fm
        on r1.film_id = fm.film_id
    group by r1.actor_id, r2.actor_id
    order by casted_together desc
    limit 1
    ) s
    join actor f1 on f1.actor_id = s.actor_a_id
    join actor s2 on s2.actor_id = s.actor_b_id
    join film fm on fm.film_id = any(s.film_ids)

# Results
![Task_1_grouping_multiple_tables](Schema&Results/Task_1_Result.png)
