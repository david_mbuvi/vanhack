## TASK 3
    Create a query to calculate how many hours each department has clocked per day.
    Order results by day then by department_name.
    Assume all shift hours are allocated to the login day, even if the logout is after midnight.

## Table Schema
![Table_schema](Schema&Results/Task_3_Schema_Table.png)

# Query Schema
   - Resulting Query
      - Hours per department that has been clocked per day.
     

    NAME                  | Type
    ----------------------+--------------
    day                   | DATE
    department_name       | VARCHAR(100)
    total_hours           | INT




# Testing Info

- **Dynamic Table Data**
    - The data within each table is re-generated each time you submit. Do not expect to get the same data back twice. 
    - In order to check accuracy of results, make sure to compare the actual results to the expected.

## Test Errors
  - Tests are done using Ruby, which is why you may see some non-SQL looking errors if something isn't correct

*************************SOLUTION QUERY*************************

    SELECT date(ts.login) as day, 
    d.name as department_name, 
    sum(DATE_PART('hour',ts.logout-ts.login)) as total_hours
    from timesheet ts
    join department d on ts.department_id = d.id
    group by d.name,day
    order by day,department_name




