![][VanHack]
# VanHack Database solutions
Solutions for [VanHack](https://vanhack.com/) PostgreSQL.

Files only include my solution tests taken from vanHack.

## Author
Written by [David Mbuvi](https://www.linkedin.com/in/david-mbuvi/)

## Executing
Can be run in browser or [Postgresql 12+] 

> Note:

```bash
 *.Go
 *.sql{MySQL/PostgreSQL}
 *.js
 *.ts
```


[VanHack]: https://vanhack.com/static-desktop/bc0f9a39d96ccebe55f6503d0cb16121.svg
