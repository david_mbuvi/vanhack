# Task 4
    For this challenge your task is to create a VIEW. This VIEW is used by a sales store to give out vouchers
    to members who have spent over $1000 in departments that have brought in more than $10000 total ordered by
    the members id. The VIEW must be called members_approved_for_voucher.
    Create a SELECT query using this view that extracts the expected results.


## Table Schema
![Table_schema](Schema&Results/Task_3_Schema_Table.png)


# sales
   - The related table for departments, products and members
   
    NAME                  | Type
    ----------------------+--------------
    id                    | INT
    department_id         | INT
    product_id            | INT
    member_id             | INT
    transaction_date      | DATE



# Query Schema
   - Resulting query
      - A table of members who have met the criteriea above

    NAME                  | Type
    ----------------------+--------------
    id                    | INT
    name                  | VARCHAR(100)
    email                 | VARCHAR(100)
    member_id             | INT
    total_spending        | DECIMAL


*************************SOLUTION QUERY*************************


    CREATE VIEW members_approved_for_voucher AS
    SELECT m.id,
    m.name,
    m.email,
    SUM(p.price) AS total_spending
    FROM members m
    JOIN sales s ON s.member_id = m.id
    JOIN products p ON p.id = s.product_id
    WHERE s.department_id IN (
    SELECT s.department_id
    FROM sales s
    JOIN products p ON p.id = s.product_id
    GROUP BY s.department_id
    HAVING SUM(p.price) > 10000
    )
    GROUP BY m.id, m.name, m.email
    HAVING SUM(p.price) > 1000
    ORDER BY m.id;

    SELECT * FROM members_approved_for_voucher;


## RESULTS
![Task_4_results](Schema&Results/Task_4_results.png)
