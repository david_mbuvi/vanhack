# Task 2
    For this challenge your task is to create a simple SELECT statement that will return all columns from the people table,
    then join that result set to the sales table so that you can return the COUNT of all sales and RANK each person by
    their sale_count. In case of ties, sort by id ascending.

## Table Schemas

# Products
   - A table of people and their IDs

    NAME                | Type
    --------------------+----------------
    id                  | INT
    name                | VARCHAR(100)



# Sale
   - A list of sales and associated details
   
    NAME            | Type
    ----------------+--------------
    id              | INT
    people_id       | INT
    sale	        | TEXT
    price           | INT


# Query Schema
   - Resulting query
      - A ranking of people by their sales


    NAME            | Type
    ----------------+--------------
    id              | INT
    name            | VARCHAR(100)
    sale_count	    | INT



*************************SOLUTION QUERY*************************

--- Create your SELECT statement here ---

     WITH CTE AS
      (
    SELECT p.id, p.name, count(t.sale) as sale_count,
        RANK() Over (Order By  count(t.sale) DESC) as sale_rank
    FROM people p
    JOIN sales t ON p.id = t.people_id
    GROUP BY p.id, p.name
    )
    SELECT *
    FROM CTE
    ORDER BY CASE when sale_count != sale_count then sale_rank END DESC

## RESULT
![Task_2_Rank_results](Schema&Results/Task_2_Result.png)
